DIR = save-preview-to-html
ADDON = save-preview-to-html.ankiaddon

$(ADDON): $(DIR)
	cd $(DIR) && zip -r ../$@ *

clean:
	rm -f $(ADDON)
