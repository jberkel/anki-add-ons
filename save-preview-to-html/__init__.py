from anki.hooks import addHook
from PyQt5.QtWebEngineWidgets import QWebEngineDownloadItem
from PyQt5.QtCore import QStandardPaths
from PyQt5.QtWidgets import QFileDialog
from os import path

def contextMenuEvent(webview, menu):
    def saveHTML():
        options = QFileDialog.Options()
        download = QStandardPaths.writableLocation(QStandardPaths.DownloadLocation)
        preview = path.join(download, 'preview')

        file_name, _ = QFileDialog.getSaveFileName(menu,
            caption='Save HTML',
            directory=preview,
            filter='All Files (*);;HTML Files (*.html)',
            options=options)
        if file_name:
            webview.page().save(file_name,
                format=QWebEngineDownloadItem.CompleteHtmlSaveFormat)

    if webview.title == 'default':
        action = menu.addAction('Save HTML')
        action.triggered.connect(saveHTML)

addHook('AnkiWebView.contextMenuEvent', contextMenuEvent)
