## Save preview to HTML

Adds a "Save HTML" menu to the preview view, which lets you
save the whole page (including CSS, Javascript and images) to a local file.

This is especially useful for debugging complex HTML templates.

![screenshot](screenshot.png)
